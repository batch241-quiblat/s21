let wrestlers = ["Dwayne Johnson", "Steve Austin", "Kurt Angle", "Dave Bautista"];
let itemFound;
let removedWrestlers;
let isUsersEmpty;

console.log("Original Array:");
console.log(wrestlers);


function addWrestler(wrestler){
	wrestlers[wrestlers.length] = wrestler;
}
addWrestler("John Cena");
console.log(wrestlers);


function getWrestler(index){
	return wrestlers[index];
}
itemFound =  getWrestler(2);
console.log(itemFound);


function deleteWrestler(){
	let lastIndex = wrestlers[wrestlers.length-1];
	wrestlers.length = wrestlers.length-1;
	return lastIndex;
}
removedWrestlers = deleteWrestler();
console.log(removedWrestlers);
console.log(wrestlers);


function updateWrestler(update, index){
	wrestlers[index] = update;
}
updateWrestler("Triple H", 3);
console.log(wrestlers);


function emptyWrestlers() {
	wrestlers.length = wrestlers.length-wrestlers.length;
}
emptyWrestlers();
console.log(wrestlers);


function checkArrayIfEmpty(){
	if (wrestlers.length > 0) {
		return false;
	}else{
		return true;
	}
}
isUsersEmpty = checkArrayIfEmpty();
console.log(isUsersEmpty);